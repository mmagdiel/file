<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Region;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\FWI */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fwi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'data_rank')->textInput() ?>

    <?= $form->field($model, 'data_position')->textInput() ?>

    <?= $form->field($model, 'by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_quantity')->textInput() ?> 
 
    <?= $form->field($model, 'status')->dropDownList([ 'Libre' => 'Libre', 'Parcialmente Libre' => 'Parcialmente Libre', 'No Libre' => 'No Libre', ], ['prompt' => '']) ?> 

    <?= DatePicker::widget([
        'model' => $model,
        'attribute' => 'from',
        'template' => '{addon}{input}',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
    ]);?>

    <?= $form->field($model, 'type')->dropDownList([ 'Proyectado' => 'Proyectado', 'Reportado' => 'Reportado', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'trend')->dropDownList([ 'Aumenta' => 'Aumenta', 'Disminuye' => 'Disminuye', 'Se Mantiene' => 'Se Mantiene', 'Sin Registro' => 'Sin Registro', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'comments')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'region_id')->dropDownList(
        ArrayHelper::map(Region::find()->all(),'id', 'name'),
        ['prompt'=>'Seleccione una region'])->label(Yii::t('app','Region Name'))  
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
