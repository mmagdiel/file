<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use kartik\popover\PopoverX;
use kartik\icons\Icon;
Icon::map($this);  
$meta = ArrayHelper::index($meta,'acronym');

/* @var $this yii\web\View */
/* @var $model common\models\Region */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Comercial'), 'url' => ['site/comercial']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <h3><a class ='btn btn-success' href = <?= Url::to(['region/comercial', 'id' => $id]) ?> >Ver detalles</a></h3>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['mta']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['mta']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['mta']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    
    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tmta ?>
            <a href = <?= isset($model->mta) ? Url::to(['region/mta', 'id' => $model->mta->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a> 
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['mdm']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['mdm']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['mdm']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tmdm ?>
            <a href = <?= isset($model->mdm) ? Url::to(['region/mdm', 'id' => $model->mdm->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>  
            <br>
            <em><?= $model->mdm ? $model->mdm->commentss : '' ?></em>    
        </div>   
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['msm']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['msm']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['msm']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tmsm ?>
            <a href = <?= isset($model->msm) ? Url::to(['region/msm', 'id' => $model->msm->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a> 
            <br>
            <em><?= $model->msm ? $model->msm->commentss : '' ?></em> 
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['fdi']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['fdi']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['fdi']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tfdi ?>
            <a href = <?= isset($model->fdi) ? Url::to(['region/fdi', 'id' => $model->fdi->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a> 
            <br>
            <em><?= $model->fdi ? $model->fdi->commentss : '' ?></em> 
        </div>
    </div>

</div>
