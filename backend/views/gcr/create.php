<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GCR */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('GCR');
$this->params['breadcrumbs'][] = ['label' => Html::title('GCR'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gcr-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
