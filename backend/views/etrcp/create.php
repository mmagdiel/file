<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ETRCP */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('ETRCP');
$this->params['breadcrumbs'][] = ['label' => Html::title('ETRCP'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="etrcp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
