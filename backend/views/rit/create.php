<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RIT */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('RIT');
$this->params['breadcrumbs'][] = ['label' => Html::title('RIT'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
