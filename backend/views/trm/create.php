<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TRM */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('TRM');
$this->params['breadcrumbs'][] = ['label' => Html::title('TRM'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trm-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
