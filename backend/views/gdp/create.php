<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GDP */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('GDP');
$this->params['breadcrumbs'][] = ['label' => Html::title('GDP'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gdp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
