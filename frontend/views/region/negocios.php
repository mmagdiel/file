<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\popover\PopoverX;
$meta = ArrayHelper::index($meta,'acronym');

/* @var $this yii\web\View */
/* @var $model common\models\Region */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Negocios'), 'url' => ['site/negocios']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Detalles'), 'url' => ['region/tnegocios', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['cci']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['cci']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['cci']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataCci,
        'columns' => [
            
            'data_percentage',
            'by',
            'from',
            'type',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/cci', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['ici']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['ici']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['ici']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataIci,
        'columns' => [
            
            'data_percentage',
            'by',
            'from',
            'type',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/ici', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>   

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['gcr']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['gcr']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['gcr']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataGcr,
        'columns' => [
            
            'data_rank',
            'data_position',
            'by',
            'from',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/gcr', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>   

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['mpfdb']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['mpfdb']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['mpfdb']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataMpfdb,
        'columns' => [
            
            'descriptive:textShort',
            'by:textShort',
            'from',
            'comments',    
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/mpfdb', 'id' => $model->id]);
                    }
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['dbi']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['dbi']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['dbi']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataDbi,
        'columns' => [
            
            'data_rank',
            'data_position',
            'by',
            'from',
            'data_position_score',
            'data_rank_score',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/dbi', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>   

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['grdi']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['grdi']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['grdi']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataGrdi,
        'columns' => [
            
            'data_rank',
            'data_position',
            'by',
            'from',   
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/grdi', 'id' => $model->id]);
                    }
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
