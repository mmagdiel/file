<?php

namespace backend\controllers;

use Yii;
use common\models\Salud;
use backend\models\search\SaludSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SaludController implements the CRUD actions for Salud model.
 */
class SaludController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Salud models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SaludSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Salud model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Salud model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            throw new UnauthorizedHttpException("Error!!!!");
        }

        $model = new Salud();
        $this->handle($model);
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Salud model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            throw new UnauthorizedHttpException("Error!!!!");
        }

        $model = $this->findModel($id);
        $this->handle($model);
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Salud model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Salud model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Salud the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Salud::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Upload file to the system
     */ 
    protected function handle(Salud $model)
    {
        if ($model->load(Yii::$app->request->post()) ) {
            $model->file = UploadedFile::getInstance($model,'file');
            if (!isset($model->file)) {
                throw new NotFoundHttpException("Error Processing the File");
            }
            $ext = $model->file->extension;
            print("<pre>".print_r($ext,true)."</pre>");
            if(strcmp($ext,'pdf') != 0 ) {
                throw new UnsupportedMediaTypeHttpException("Error Processing Request");
            }
            $token = Yii::$app->security->generateRandomString(23);
            $model->path = 'pdf/salud/' . $token . '.' . $ext;
            $model->file->saveAs($model->path);
            $model->error = $model->file->error;
            if ($model->error != '0') {
                throw new NotFoundHttpException("Error Saved the File");
            }
            $model->size = $model->file->size;
            $model->name = $model->file->name;
            if ($model->save(false) ) {
                $id = $model->id;
                return $this->redirect(['view', 
                   'id' => $id ]);                  
            }
        } 
    }
}
