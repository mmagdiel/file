<?php

/* @var $this yii\web\View */
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
Icon::map($this);  
$this->title = 'Ficha Informativa de Paises';

?>
<div class="site-index">

    <div class="jumbotron">
        <?= Html::img('@web/img/logo.jpg', ['alt'=>Yii::$app->name, 'height' => '257', 'width' => '257']); ?>
    <h2><?= $this->title?> </h2>
    </div>


    <div class="body-content">

        <div class="row">
            <div class="col-lg-3">
                <h3>Datos Generales</h3>

                <p class="text-justify">Información despcritiva de los principales datos geograficos y generales de los paises en analisis.</p>

            </div>
            <div class="col-lg-4">
                <h3>Informacion Economica y de Negocios</h3>

                <p class="text-justify">Muestra la información de una serie de indicadores estadísticos, de vital importancia para la toma de desiciones en materia econónica, de negocio y financiera basada en informacion de primera mano.</p>

            </div>
            <div class="col-lg-4">
                <h3>Reporte</h3>

                <p class="text-justify">Conglomerado de informes que reportan sitiación del mercado de salud, politícas macroeconomicas y el mercado competitivo en general y utilizando información estadistica actualizada de fuentes oficiales añadiendo un analisis socio-econónico y socio-político. </p>

            </div>
            <div class='col-lg-1'>
            <br>
                <p><a class="btn btn-success" href= <?= Url::to('@web/site/informes') ?> >Visualizar</a></p>

            </div>
        </div>

    </div>
</div>
