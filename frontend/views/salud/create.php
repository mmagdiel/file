<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Salud */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('Salud');
$this->params['breadcrumbs'][] = ['label' => Html::title('Salud'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salud-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
