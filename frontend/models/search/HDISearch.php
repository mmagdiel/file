<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Hdi;

/**
 * HDISearch represents the model behind the search form about `common\models\HDI`.
 */
class HDISearch extends Hdi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'total_quantity', 'data_position', 'created_at', 'created_by', 'updated_at', 'updated_by', 'region_id'], 'integer'],
            [['data'], 'number'],
            [['percentile', 'by', 'from', 'type', 'trend', 'comments'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Hdi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'data' => $this->data,
            'total_quantity' => $this->total_quantity, 
            'data_position' => $this->data_position, 
            'from' => $this->from,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'region_id' => $this->region_id,
        ]);

        $query->andFilterWhere(['like', 'percentile', $this->percentile])
            ->andFilterWhere(['like', 'by', $this->by])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'trend', $this->trend])
            ->andFilterWhere(['like', 'comments', $this->comments]);

        return $dataProvider;
    }
}
