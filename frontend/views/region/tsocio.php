<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use kartik\popover\PopoverX;
use kartik\icons\Icon;
Icon::map($this);  
$meta = ArrayHelper::index($meta,'acronym');

/* @var $this yii\web\View */
/* @var $model common\models\Region */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sociopolíticos'), 'url' => ['site/socio']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <h3><a class ='btn btn-success' href = <?= Url::to(['region/socio', 'id' => $id]) ?> >Ver detalles</a></h3>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['hdi']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['hdi']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['hdi']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->thdi ?>
            <a href = <?= isset($model->hdi) ? Url::to(['region/hdi', 'id' => $model->hdi->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->hdi ? $model->hdi->comments : '' ?></em>        
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['fwi']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['fwi']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['FWI']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tfwi ?>
            <a href = <?= isset($model->fwi) ? Url::to(['region/fwi', 'id' => $model->fwi->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->fwi ? $model->fwi->comments : '' ?></em>    
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['gpi']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['gpi']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['gpi']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tgpi ?>
            <a href = <?= isset($model->gpi) ? Url::to(['region/gpi', 'id' => $model->gpi->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->gpi ? $model->gpi->commentss : '' ?></em>    
        </div>  
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['whr']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['whr']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['whr']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->twhr ?>
            <a href = <?= isset($model->whr) ? Url::to(['region/whr', 'id' => $model->whr->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->whr ? $model->whr->commentss : '' ?></em>
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['di']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['di']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['di']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tdi ?>
            <a href = <?= isset($model->di) ? Url::to(['region/di', 'id' => $model->di->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->di ? $model->di->commentss : '' ?></em>
        </div>
    </div>        

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['pci']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['pci']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['pci']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tpci ?>
            <a href = <?= isset($model->pci) ? Url::to(['region/pci', 'id' => $model->pci->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->pci ? $model->pci->commentss : '' ?></em>
        </div>
    </div>        

</div>
