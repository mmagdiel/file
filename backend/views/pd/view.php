<?php

use common\components\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\PD */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Html::title('PD'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pd-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'data',
            'data_unit',
            'data_percentage',
            'data_local',
            'data_local_unit',
            'data_local_percentage',
            'by',
            'from',
            'type',
            'trend',
            'comments',
            'created_at:datetime',
            [
                'attribute'=>'created_by',
                'value'=>function($dataProvider){
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            'updated_at:datetime',
            [
                'attribute'=>'updated_by',
                'value'=>function($dataProvider){
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            [
                'attribute' => 'region_id',
                'label' => Yii::t('app', 'Region Name'),
                'value' => function($model){
                    return $model->region->name;
                },
            ]
        ],
    ]) ?>

</div>
