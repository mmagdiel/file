<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "salud".
 *
 * @property string $id
 * @property string $path
 * @property string $name
 * @property string $size
 * @property integer $error
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $region_id
 *
 * @property Region $region
 */
class Salud extends \yii\db\ActiveRecord
{
    /**
     *
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'salud';
    }

    /**
     *  Public doc   
     */    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className()
        ];
    }    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['size', 'error', 'created_at', 'created_by', 'updated_at', 'updated_by', 'region_id'], 'integer'],
            [['region_id'], 'required'],
            [['path'], 'string', 'max' => 37],
            [['name'], 'string', 'max' => 257],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'path' => Yii::t('app', 'Path'),
            'name' => Yii::t('app', 'Name'),
            'size' => Yii::t('app', 'Size'),
            'error' => Yii::t('app', 'Error'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'region_id' => Yii::t('app', 'Region ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @inheritdoc
     * @return SaludQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\SaludQuery(get_called_class());
    }
}
