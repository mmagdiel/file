<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ML */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('ML');
$this->params['breadcrumbs'][] = ['label' => Html::title('ML'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ml-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
