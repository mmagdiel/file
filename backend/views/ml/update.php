<?php

use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ML */

$this->title = Yii::t('app', 'Update') . ' ' . Html::title('ML');
$this->params['breadcrumbs'][] = ['label' => Html::title('ML'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ml-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
