<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MPFDB */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('MPFDB');
$this->params['breadcrumbs'][] = ['label' => Html::title('MPFDB'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mpfdb-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
