<?php

use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\STR */

$this->title = Yii::t('app', 'Update') . ' ' . Html::title('STR');
$this->params['breadcrumbs'][] = ['label' => Html::title('STR'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="str-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
