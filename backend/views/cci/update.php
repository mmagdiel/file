<?php

use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CCI */

$this->title = Yii::t('app', 'Update') . ' ' . Html::title('CCI');
$this->params['breadcrumbs'][] = ['label' => Html::title('CCI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cci-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
