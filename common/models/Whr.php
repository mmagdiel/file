<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "whr".
 *
 * @property string $id
 * @property integer $data_rank
 * @property integer $data_position
 * @property string $by
 * @property string $from
 * @property string $type
 * @property string $trend
 * @property string $comments
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $region_id
 *
 * @property Region $region
 */
class Whr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'whr';
    }

    /**
     *  Public doc   
     */    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_rank', 'data_position', 'score', 'created_at', 'created_by', 'updated_at', 'updated_by', 'region_id'], 'integer'],
            [['by', 'from', 'type', 'trend', 'region_id'], 'required'],
            [['from'], 'safe'],
            [['type', 'trend'], 'string'],
            [['by'], 'string', 'max' => 127],
            [['comments'], 'string', 'max' => 397],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'data_rank' => Yii::t('app', 'Data Rank'),
            'data_position' => Yii::t('app', 'Data Position'),
            'score' => Yii::t('app', 'Score'), 
            'by' => Yii::t('app', 'By'),
            'from' => Yii::t('app', 'From'),
            'type' => Yii::t('app', 'Type'),
            'trend' => Yii::t('app', 'Trend'),
            'comments' => Yii::t('app', 'Comments'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'region_id' => Yii::t('app', 'Region ID'),
        ];
    }

    /**
     *
     */
    public static function de()
    {
        return 'No se dispone de información.';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     *
     */
    public function getRanks()
    {
        $temp = $this->data_rank;
        $t = $this->data_position;
        if ( !is_null($temp) ) {
            if ( !is_null($t) ) {
                $text = $this->data_position . ' de ' . $this->data_rank;
            return $text;
            } else {
                return self::de();
            }
        } else {
            return self::de();
        }
    }

    /**
     *
     */
    public function getCommentss()
    {
        $this->comments ? $a = '* ' . $this->comments : $a = $this->comments;
        return $a;
    }

    /**
     * @inheritdoc
     * @return \common\models\query\WhrQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\WhrQuery(get_called_class());
    }
}
