<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PCI */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('PCI');
$this->params['breadcrumbs'][] = ['label' => Html::title('PCI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pci-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
