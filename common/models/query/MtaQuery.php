<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\MTA]].
 *
 * @see \common\models\MTA
 */
class MtaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     *
     */
    public function current()
    {
        return $this->addOrderBy('created_at DESC');
    }

    /**
     * @inheritdoc
     * @return \common\models\MTA[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\MTA|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
