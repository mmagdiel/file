<?php

namespace common\models;

use Yii;
use kartik\icons\Icon;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "region".
 *
 * @property string $id
 * @property string $name
 * @property string $capital
 * @property string $area
 * @property string $map
 * @property string $flag
 * @property string $north
 * @property string $east
 * @property string $west
 * @property string $south
 * @property string $north_east
 * @property string $north_west
 * @property string $south_east
 * @property string $south_west
 * @property integer $border
 * @property integer $coastline
 * @property string $internet_domain
 * @property integer $area_code
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $currency_id
 *
 * @property Bt[] $bts
 * @property Cci[] $ccis
 * @property Dbi[] $dbis
 * @property Di[] $dis
 * @property Etrcp[] $etrcps
 * @property Fd[] $fds
 * @property Fdi[] $fdis
 * @property Fwi[] $fwis
 * @property Gdp[] $gdps
 * @property Gpi[] $gpis
 * @property Grdi[] $grdis
 * @property Hdi[] $hdis
 * @property Ici[] $icis
 * @property Ier[] $iers
 * @property Inr[] $inrs
 * @property Ir[] $irs
 * @property Macro[] $macros 
 * @property Mdm[] $mdms
 * @property Mercado[] $mercados 
 * @property Ml[] $mls
 * @property Mpfdb[] $mpfdbs
 * @property Ms[] $ms
 * @property Msm[] $msms
 * @property Mta[] $mtas
 * @property Mw[] $mws
 * @property PcGdp[] $pcGdps
 * @property PcPd[] $pcPds
 * @property PcPds[] $pcPds0
 * @property Pci[] $pcis
 * @property Pd[] $pds
 * @property Pds[] $pds0
 * @property Population[] $populations
 * @property Currency $currency
 * @property Rit[] $rits
 * @property Rvt[] $rvts
 * @property Salud[] $saluds 
 * @property Str[] $strs
 * @property Trm[] $trms
 * @property Ur[] $urs
 * @property Whr[] $whrs
 */
class Region extends \yii\db\ActiveRecord
{  

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region';
    }

    /**
     *
     */
    public function behaviors()
    {
        return [
            'imageUploaderBehavior' => [
                'class' => 'demi\image\ImageUploaderBehavior',
                'imageConfig' => [
                    // Name of image attribute where the image will be stored
                    'imageAttribute' => 'flag',
                    // Yii-alias to dir where will be stored subdirectories with images
                    'savePathAlias' => '@public/images/flag',
                    // Yii-alias to root project dir, relative path to the image will exclude this part of the full path
                    'rootPathAlias' => '@public/',
                    // Name of default image. Image placed to: webrooot/images/{noImageBaseName}
                    // You must create all noimage files: noimage.jpg, medium_noimage.jpg, small_noimage.jpg, etc.
                    'noImageBaseName' => 'noimage.jpg',
                    // List of thumbnails sizes.
                    // Format: [prefix=>max_width]
                    // Thumbnails height calculated proportionally automatically
                    // Prefix '' is special, it determines the max width of the main image
                    'imageSizes' => [
                        '' => 227,
                    ],
                    // This params will be passed to \yii\validators\ImageValidator
                    'imageValidatorParams' => [
                        'minWidth' => 400,
                        'minHeight' => 300,
                    ],
                    // Cropper config
                    'aspectRatio' => 4 / 3, // or 16/9(wide) or 1/1(square) or any other ratio. Null - free ratio
                    // default config
                    'imageRequire' => false,
                    'fileTypes' => 'jpg,jpeg,gif,png',
                    'maxFileSize' => 10485760, // 10mb
                    // If backend is located on a subdomain 'admin.', and images are uploaded to a directory
                    // located in the frontend, you can set this param and then getImageSrc() will be return
                    // path to image without subdomain part even in backend part
                    'backendSubdomain' => 'admin.',
                ],
            ],
            TimestampBehavior::className(),
            BlameableBehavior::className()
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'capital', 'area', 'border', 'area_code', 'currency_id'], 'required'],
            [['area', 'border', 'coastline', 'area_code', 'created_at', 'created_by', 'updated_at', 'updated_by', 'currency_id'], 'integer'],
            [['map'], 'string'],
            [['name'], 'string', 'max' => 97],
            [['capital', 'north', 'east', 'west', 'south', 'north_east', 'north_west', 'south_east', 'south_west'], 'string', 'max' => 127],
            [['flag'], 'string', 'max' => 251],
            [['internet_domain'], 'string', 'max' => 3],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'capital' => Yii::t('app', 'Capital'),
            'area' => Yii::t('app', 'Area'),
            'map' => Yii::t('app', 'Map'),
            'flag' => Yii::t('app', 'Flag'),
            'north' => Yii::t('app', 'North'),
            'east' => Yii::t('app', 'East'),
            'west' => Yii::t('app', 'West'),
            'south' => Yii::t('app', 'South'),
            'north_east' => Yii::t('app', 'North East'),
            'north_west' => Yii::t('app', 'North West'),
            'south_east' => Yii::t('app', 'South East'),
            'south_west' => Yii::t('app', 'South West'),
            'border' => Yii::t('app', 'Border'),
            'coastline' => Yii::t('app', 'Coastline'),
            'internet_domain' => Yii::t('app', 'Internet Domain'),
            'area_code' => Yii::t('app', 'Area Code'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'currency_id' => Yii::t('app', 'Currency ID'),
        ];
    }

    /**
     *
     */
    public static function de()
    {
        return 'No se dispone información.';
    }


    /**
     *
     */
    public static function type($status) 
    {
        if ($status == 'Reportado') {
            return Icon::show('list-alt', ['class' => 'fa-fw'], Icon::FA) . '(Reportado)'; 
        }
        if ($status == 'Proyectado') {
            return Icon::show('chart', ['class' => 'fa-fw'], Icon::FA) . '(Proyectado)'; 
        }
    }

    /**
     *
     */
    public static function trend($status) 
    {
        if ( $status == 'Aumenta' ) {
            return ' aumento ' . Icon::show('arrow-circle-o-up', ['class' => 'fa-fw'], Icon::FA); 
        }
        if ( $status == 'Disminuye' ) {
            return ' disminución ' . Icon::show('arrow-circle-o-down', ['class' => 'fa-fw'], Icon::FA); 
        }
        if ( $status == 'Se Mantiene' ) {
            return ' estabilidad ' . Icon::show('repeat', ['class' => 'fa-fw'], Icon::BSG);
        }
        if ( $status == 'Sin Registro' ) {
            return ' ausencia de información ' . Icon::show('remove', ['class' => 'fa-fw'], Icon::BSG) . ' en comparación con el último registro '; 
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMacros() 
    { 
       return $this->hasMany(Macro::className(), ['region_id' => 'id']); 
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMercados() 
    { 
        return $this->hasMany(Mercado::className(), ['region_id' => 'id']); 
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBts()
    {
        return $this->hasMany(Bt::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBt()
    {
        return $this->hasOne(Bt::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTbt()
    {
        $temp = $this->bt;
        if ( !is_null($temp) ) {
            $text = 'Según ' . 
                $this->bt->by 
                . ' para la fecha '.
                $this->bt->from
                .', la balanza comercial  de ' .
                $this->bt->region->name 
                . ' se ubicó en ' .
                $this->bt->datas
                . ', representando esto un ' .
                self::trend($this->bt->trend);
            return $text;
        } else {
            return self::de();
        }
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getSaluds() 
    { 
        return $this->hasMany(Salud::className(), ['region_id' => 'id']); 
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCcis()
    {
        return $this->hasMany(Cci::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCci()
    {
        return $this->hasOne(Cci::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTcci()
    {
        $temp = $this->cci;
        if ( !is_null($temp) ) {
            $text = 'El Índice de Confianza de Consumidor de ' .
                $this->cci->region->name
                . ' fue de ' .
                Yii::$app->formatter->asPercent($this->cci->data_percentage/100,2)
                . ' para la fecha ' . 
                $this->cci->from
                . ', dato que ' .
                self::trend($this->cci->trend)
                . ' y ha sido ' .
                self::type($this->cci->type)
                . ' por ' .
                $this->cci->by;
            return $text;
         } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDbis()
    {
        return $this->hasMany(Dbi::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDbi()
    {
        return $this->hasOne(Dbi::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTdbi()
    {
        $temp = $this->dbi;
        if ( !is_null($temp) ) {
            $text = 'Para el Informe de Doing Buissines de ' . 
                $this->dbi->from
                . ', ' . 
                $this->dbi->region->name
                . ' se ubicó en la posición '.
                $this->dbi->ranks 
                . ' países. Dicha información fue ' .
                self::type($this->dbi->type);
            return $text;
         } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDis()
    {
        return $this->hasMany(Di::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDi()
    {
        return $this->hasOne(Di::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTdi()
    {
        $temp = $this->di;
        if ( !is_null($temp) ) {
            $text = 'El Indice de Democracia de ' . 
                $this->di->region->name
                . ' lo  ubicó en la posición ' .
                $this->di->ranks
                . ' países. Dicha información fue ' .
                self::type($this->di->type)
                . ' por ' . 
                $this->di->by
                . ' en ' .
                $this->di->from;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEtrcps()
    {
        return $this->hasMany(Etrcp::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEtrcp()
    {
        return $this->hasOne(Etrcp::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTetrcp()
    {
        $temp = $this->etrcp;
        if ( !is_null($temp) ) {
            $text = 'La tarifa efectiva sobre las utilidades en ' . 
                $this->etrcp->region->name 
                . ' se ubica en ' . 
                Yii::$app->formatter->asPercent($this->etrcp->data_percentage/100,2)
                . ' desde ' . 
                $this->etrcp->from
                . ', cifra que representa ' .
                self::trend($this->etrcp->trend)
                . ' ' .
                self::type($this->etrcp->type)
                . ', según información oficial de ' . 
                $this->etrcp->by;
            return $text;
         } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFds()
    {
        return $this->hasMany(Fd::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFd()
    {
        return $this->hasOne(Fd::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTfd()
    {
        $temp = $this->fd;
        if ( !is_null($temp) ) {
            $text = 'El deficit Fiscal de ' .
                $this->fd->region->name 
                . ' para la fecha ' .
                $this->fd->from 
                . ' ha sido de ' . 
                Yii::$app->formatter->asPercent($this->fd->data_percentage/100,2)
                . ' del PIB, representando así un ' .
                self::trend($this->fd->trend)
                . ' con relación al año anterior';
            return $text;
         } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFdis()
    {
        return $this->hasMany(Fdi::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFdi()
    {
        return $this->hasOne(Fdi::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTfdi()
    {
        $temp = $this->fdi;
        if ( !is_null($temp) ) {
            $text = 'Según ' .
                $this->fdi->by 
                . ' para la fecha ' .
                $this->fdi->from
                . ', la inversión extranjera directa de ' . 
                $this->fdi->region->name
                . ' fue ' . 
                $this->fdi->datas
                . ', representando esto un ' .
                self::trend($this->fdi->trend);
            return $text;
         } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFwis()
    {
        return $this->hasMany(Fwi::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFwi()
    {
        return $this->hasOne(Fwi::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTfwi()
    {
        $temp = $this->fwi;
        if ( !is_null($temp) ) {
            $text = 'El Índice de Freedom in the world para ' . 
                $this->fwi->region->name
                . ', fue de ' .
                $this->fwi->total_quantity
                . ', ubicando en la posición ' .
                $this->fwi->ranks
                . ' lo cual representa ' .
                self::trend($this->fwi->trend)
                . ', según ' .
                $this->fwi->by
                . ' informe ' . 
                self::type($this->fwi->type)
                . ' en ' .
                $this->fwi->from;
            return $text;
         } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGdps()
    {
        return $this->hasMany(Gdp::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGdp()
    {
        return $this->hasOne(Gdp::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTgdp()
    {
        $temp = $this->gdp;
        if ( !is_null($temp) ) {
            $text = 'El producto Interno Bruto de ' .
                $this->gdp->region->name
                . ' para ' .
                $this->gdp->from
                . ' tuvo cambios porcentuales de ' .
                Yii::$app->formatter->asPercent($this->gdp->data_percentage/100,2)
                . ' lo que representa un ' .
                self::trend($this->gdp->trend)
                . ' según ' .
                $this->gdp->by;
            return $text;
         } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGpis()
    {
        return $this->hasMany(Gpi::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGpi()
    {
        return $this->hasOne(Gpi::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTgpi()
    {
        $temp = $this->gpi;
        if ( !is_null($temp) ) {
            $text = 'El Índice Global de Paz de ' .  
                $this->gpi->region->name
                . ' lo ubicó en la posición ' .
                $this->gpi->ranks
                . ' países. Dicha información fue ' . 
                self::type($this->gpi->type)
                . ' por ' .
                $this->gpi->by
                . ' en ' .
                $this->gpi->from;
            return $text;
         } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGcrs()
    {
        return $this->hasMany(Gcr::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGcr()
    {
        return $this->hasOne(Gcr::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTgcr()
    {
        $temp = $this->gcr;
        if ( !is_null($temp) ) {
            $text = 'El Índice Global de Paz de ' .  
                $this->gcr->region->name
                . ' lo ubicó en la posición ' .
                $this->gcr->ranks
                . ' países. Dicha información fue ' . 
                self::type($this->gcr->type)
                . ' por ' .
                $this->gcr->by
                . ' en ' .
                $this->gcr->from;
            return $text;
         } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrdis()
    {
        return $this->hasMany(Grdi::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrdi()
    {
        return $this->hasOne(Grdi::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTgrdi()
    {
        $temp = $this->grdi;
        if ( !is_null($temp) ) {
            $text = 'Usando el análisis de Global Retail Development Index ' . 
                $this->grdi->region->name
                . ' se ubicó en la posición ' .
                $this->grdi->ranks
                . ' países. Dicha información fue '. 
                self::type($this->grdi->type)
                . ' en ' .
                $this->grdi->from
                . ' , según ' .
                $this->grdi->by;
            return $text;
         } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHdis()
    {
        return $this->hasMany(Hdi::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHdi()
    {
        return $this->hasOne(Hdi::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getThdi()
    {
        $temp = $this->hdi;
        if ( !is_null($temp) ) {
            $text = 'El IDH para ' .
                $this->hdi->region->name 
                . ' según ' . 
                $this->hdi->by 
                . ' fue de ' .
                $this->hdi->data 
                . ' en ' . 
                $this->hdi->from
                .', siendo este ' . 
                $this->hdi->percentile;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIcis()
    {
        return $this->hasMany(Ici::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIci()
    {
        return $this->hasOne(Ici::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTici()
    {
        $temp = $this->ici;
        if ( !is_null($temp) ) {
            $text = 'El indice de Confianza Industrial de la ' .
                $this->ici->region->name 
                . ' fue de ' . 
                Yii::$app->formatter->asPercent($this->ici->data_percentage/100,2)
                . ' para la fecha de ' . 
                $this->ici->from 
                . ' data que ' .
                self::trend($this->ici->trend)
                . ' y ha sido ' . 
                self::type($this->ici->type) 
                . ' por ' . 
                $this->ici->by;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIers()
    {
        return $this->hasMany(Ier::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIer()
    {
        return $this->hasOne(Ier::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTier()
    {
        $temp = $this->ier;
        if ( !is_null($temp) ) {
            $text = 'La tasa de cambio se ubico en ' .  
                $this->ier->locals
                . ' segun ' . 
                $this->ier->by
                . ', en ' .
                $this->ier->from;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInrs()
    {
        return $this->hasMany(Inr::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInr()
    {
        return $this->hasOne(Inr::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTinr()
    {
        $temp = $this->inr;
        if ( !is_null($temp) ) {
            $text = 'Las reservas internacionales se ubicaron en ' .
                $this->inr->datas
                . ' ' . 
                self::trend($this->inr->trend)
                . ' con respecto al año anterior, según cifras oficiales de ' .
                $this->inr->by 
                . ' en ' .
                $this->inr->from;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIrs()
    {
        return $this->hasMany(Ir::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIr()
    {
        return $this->hasOne(Ir::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTir()
    {
        $temp = $this->ir;
        if ( !is_null($temp) ) {
            $text = 'La tasa de inflación de ' .
                $this->ir->region->name
                . ' tuvo un ' .
                self::trend($this->ir->trend)
                . ' de ' .
                Yii::$app->formatter->asPercent($this->ir->data_percentage/100,2)
                . ' durante ' . 
                $this->ir->from
                . ', según ' . 
                self::type($this->ir->type)
                . ' de ' . 
                $this->ir->by;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMdms()
    {
        return $this->hasMany(Mdm::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMdm()
    {
        return $this->hasOne(Mdm::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTmdm()
    {
        $temp = $this->mdm;
        if ( !is_null($temp) ) {
            $m = $this->mdms;
            $list = ' ';
            foreach ($m as $key => $value) {
                $list .= $m[$key]->name . ' con ' . 
                Yii::$app->formatter->asPercent($m[$key]->data_percentage/100,2) . ', ' ;
            }
            $text = 'Los principales paises que importan a ' .
                $this->mdm->region->name
                . ' son: ' .
                $list
                . ' Según datos de ' .
                $this->mdm->by
                . ' en ' .
                $this->mdm->from;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMls()
    {
        return $this->hasMany(Ml::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMl()
    {
        return $this->hasOne(Ml::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTml()
    {
        $temp = $this->ml;
        if ( !is_null($temp) ) {
            $text = 'Se registró liquidez monetaria de ' .
                $this->ml->locals
                . ' monto que representa un ' .
                self::trend($this->ml->trend)
                . ' para ' .
                $this->ml->region->name
                . ' en ' .
                $this->ml->from
                . ' según información ' . 
                self::type($this->ml->type)
                . ' oficial de ' . 
                $this->ml->by;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMpfdbs()
    {
        return $this->hasMany(Mpfdb::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMpfdb()
    {
        return $this->hasOne(Mpfdb::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTmpfdb()
    {
        $temp = $this->mpfdb;
        if ( !is_null($temp) ) {
            $text = 'Los factores más problemáticos para invertir y hacer negocios de ' .
                $this->mpfdb->region->name
                . ' para la fecha de '.
                $this->mpfdb->from
                . ' fueron ' .
                $this->mpfdb->descriptive
                . ', según ' . 
                $this->mpfdb->by;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMs()
    {
        return $this->hasMany(Ms::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getM()
    {
        return $this->hasOne(Ms::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTm()
    {
        $temp = $this->m;
        if ( !is_null($temp) ) {
            $text = $this->m->descriptive
                . ' información obtenida según informes de ' . 
                $this->m->by
                .' en ' . 
                $this->m->from;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMsms()
    {
        return $this->hasMany(Msm::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMsm()
    {
        return $this->hasOne(Msm::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTmsm()
    {
        $temp = $this->msm;
        if ( !is_null($temp) ) {
            $m = $this->msms;
            $list = ' ';
            foreach ($m as $key => $value) {
                $list .= $m[$key]->name . ' con ' . 
                Yii::$app->formatter->asPercent($m[$key]->data_percentage/100,2) . ', ' ;
            }
            $text = 'Los principales paises de dónde ' . 
                $this->msm->region->name
                . ' importa son: ' .
                $list
                . ' Según datos de ' . 
                $this->msm->by
                . ' en ' .
                $this->msm->from;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtas()
    {
        return $this->hasMany(Mta::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMta()
    {
        return $this->hasOne(Mta::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTmta()
    {
        $temp = $this->mta;
        //$temp = 'cfv';
        if ( !is_null($temp) ) {
            $text = $this->mta->descriptive
            . '<em>( ' . 
            $this->mta->to
            . ' ) </em>';
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMws()
    {
        return $this->hasMany(Mw::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMw()
    {
        return $this->hasOne(Mw::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTmw()
    {
        $temp = $this->mw;
        if ( !is_null($temp) ) {
            $text = 'El salario minimo de ' .
                $this->mw->region->name
                . ' se ubico en ' . 
                $this->mw->datas
                . ' que representa ' . 
                $this->mw->locals
                . ' para ' . 
                $this->mw->from;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcGdps()
    {
        return $this->hasMany(PcGdp::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcGdp()
    {
        return $this->hasOne(PcGdp::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTpcGdp()
    {
        $temp = $this->pcGdp;
        if ( !is_null($temp) ) {
            $text = 'EL PIB per capita para ' .
                $this->pcGdp->region->name
                . ', resgistró un total de ' .
                $this->pcGdp->datas
                .' esto fue ' .
                self::trend($this->pcGdp->trend)
                . ' en ' .
                $this->pcGdp->from
                . ' que representa ' .
                self::type($this->pcGdp->type)
                . ' según ' .
                $this->pcGdp->by;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcPds()
    {
        return $this->hasMany(PcPd::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcPd()
    {
        return $this->hasOne(PcPd::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTpcPd()
    {
        $temp = $this->pcPd;
        if ( !is_null($temp) ) {
            $text = 'La Deuda Pública Per Cápita de ' .
                $this->pcPd->region->name
                . ' , se manifestó en ' .
                $this->pcPd->datas
                . ' lo cual ' .
                self::trend($this->pcPd->trend)
                . ' en ' .
                $this->pcPd->from
                . ', cifra que fue ' . 
                self::type($this->pcPd->type)
                . ' según ' .
                $this->pcPd->by;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcPds0()
    {
        return $this->hasMany(PcPds::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcPds00()
    {
        return $this->hasOne(PcPds::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTpcPds00()
    {
        $temp = $this->pcPds00;
        if ( !is_null($temp) ) {
            $text = 'El servicio de deuda  Per Cápita de ' .
                $this->pcPds00->region
                . ', tuvo un valor en ' .
                $this->pcPds00->datas
                . ', lo que representa ' .
                $this->pcPds00->locals
                . ' en ' .
                $this->pcPds00->from
                . ', cifra que fue ' .
                self::type($this->pcPds00->type)
                . ' según ' .
                $this->pcPds00->by;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcis()
    {
        return $this->hasMany(Pci::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPci()
    {
        return $this->hasOne(Pci::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTpci()
    {
        $temp = $this->pci;
        if ( !is_null($temp) ) {
            $text = 'Los resultados de Índice de Percepción de la corrupción en ' .
                $this->pci->from
                . ' de ' .
                $this->pci->region->name
                . ' lo ubicaron en la posición ' .
                $this->pci->ranks
                . ' países, obteniendo una puntuación de ' .
                $this->pci->score
               . '. Ésta es la información más recientemente ' . 
                self::type($this->pci->type)
                . ' por ' . 
                $this->pci->by;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPds()
    {
        return $this->hasMany(Pd::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPd()
    {
        return $this->hasOne(Pd::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTpd()
    {
        $temp = $this->pd;
        if ( !is_null($temp) ) {
            $text = 'La Deuda Pública de ' .
                $this->pd->region->name
                . ' para ' .
                $this->pd->from
                . ' registro un valor de ' .
                $this->pd->datas
                . ' lo que representa un ' . 
                self::trend($this->pd->trend)
                . ' según ' .
                $this->pd->by;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPds0()
    {
        return $this->hasMany(Pds::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPds00()
    {
        return $this->hasOne(Pds::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTpds00()
    {
        $temp = $this->pds00;
        if ( !is_null($temp) ) {
            $text = 'El Servicio de Deuda para ' .
                $this->pds00->region->name
                . ' durante ' .
                $this->pds00->from
                . ' fue de ' .
                self::type($this->pds00->type)
                . ' siendo esto ' .
                $this->pds00->locals 
                . ' lo que representa un ' .
                self::trend($this->pds00->trend) 
                . ' según ' .
                $this->pds00->by;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPopulations()
    {
        return $this->hasMany(Population::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPopulation()
    {
        return $this->hasOne(Population::className(), ['region_id' => 'id'])->current();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTPopulation()
    {
        $temp = $this->population;
        if ( !is_null($temp) ) {
            $text = 'La población de (Región) para (Date) (Proyectada o registrada es de (data) habitantes según (Varchar)';
            $q = $this->population->trend =='Aumenta' ? 'Arriba' : 
                    $this->population->trend == 'Disminuye' ? 'Bajo' : 
                        $this->population->trend == 'Se Mantiene' ? 'Mantiene' : 'Registro';
            $t = Yii::$app->formatter->asDecimal($this->population->data) 
            . ' habitantes ' .  
            $this->population->type == 'Reportado' ? ' :) ' : ' :( ' 
            . ' por lo tanto ' . 
            $q 
            . ' Reportado por ' . 
            $this->population->by 
            . ' en ' . 
            $this->population->from;
            return $text;
        } else {
            return self::de();
        }
    }    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRits()
    {
        return $this->hasMany(Rit::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRit()
    {
        return $this->hasOne(Rit::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTrit()
    {
        $temp = $this->rit;
        if ( !is_null($temp) ) {
            $text = 'La tasa de Impuesto sobre la renta oficial para ' .
                $this->rit->region->name
                . ' se ubica en ' . 
                Yii::$app->formatter->asPercent($this->rit->data_percentage/100,2)
                . ' presentado una ' .
                self::trend($this->rit->trend)
                . ' desde ' .
                $this->rit->from
                . ', según información ' .
                self::type($this->rit->type) 
                . ' oficial de ' .
                $this->rit->by;
            return $text;
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRvts()
    {
        return $this->hasMany(Rvt::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRvt()
    {
        return $this->hasOne(Rvt::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTrvt()
    {
        $temp = $this->rvt;
        if ( !is_null($temp) ) {
            $text = 'La tasa de Impuesto al valor agregado oficial para ' . 
            $this->rvt->region->name
            . ' se ubica en ' . 
            Yii::$app->formatter->asPercent($this->rvt->data_percentage/100,2)
            . ' desde ' .
            $this->rvt->from
            . ', según información oficial de ' . 
            $this->rvt->by;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStrs()
    {
        return $this->hasMany(Str::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStr()
    {
        return $this->hasOne(Str::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTstr()
    {
        $temp = $this->str;
        if ( !is_null($temp) ) {
            $text = $this->str->descriptive
            . ' según ' .
            $this->str->by
            . ' en ' .
            $this->str->from;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrms()
    {
        return $this->hasMany(Trm::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrm()
    {
        return $this->hasOne(Trm::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTtrm()
    {
        $temp = $this->trm;
        if ( !is_null($temp) ) {
            $text = 'La tasa de cambio se ubico en ' .
                $this->trm->locals
                . ' segun ' .
                $this->trm->by
                . ', en ' .
                $this->trm->from;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUrs()
    {
        return $this->hasMany(Ur::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUr()
    {
        return $this->hasOne(Ur::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTur()
    {
        $temp = $this->ur;
        if ( !is_null($temp) ) {
            $text = 'La tasa de desempleo en ' . 
                $this->ur->region->name
                . ' se ubicó en ' . 
                Yii::$app->formatter->asPercent($this->ur->data_percentage/100,2)
                . ' lo significa ' .
                self::trend($this->ur->trend)
                . ' durante ' .
                $this->ur->from
                . ', según ' .
                self::type($this->ur->type)
                . ' de ' .
                $this->ur->by;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWhrs()
    {
        return $this->hasMany(Whr::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWhr()
    {
        return $this->hasOne(Whr::className(), ['region_id' => 'id'])->current();
    }

    /**
     *
     */
    public function getTwhr()
    {
        $temp = $this->whr;
        if ( !is_null($temp) ) {
            $text = 'Partiendo de los resultados del World Happiness Report ' .
                $this->whr->region->name
                . ' se ubicó en la posición ' .
                $this->whr->ranks
                . ' países, obteniendo una puntuación de ' . 
                $this->whr->score
                . '. Esta es la información más reciente reportada por ' .
                $this->whr->by
                . ' en ' .
                $this->whr->from;
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @inheritdoc
     * @return \common\models\query\RegionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\RegionQuery(get_called_class());
    }
}
