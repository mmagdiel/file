<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RVT */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('RVT');
$this->params['breadcrumbs'][] = ['label' => Html::title('RVT'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rvt-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
