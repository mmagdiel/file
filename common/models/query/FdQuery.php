<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\FD]].
 *
 * @see \common\models\FD
 */
class FdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     *
     */
    public function current()
    {
        return $this->addOrderBy('from DESC');
    }

    /**
     * @inheritdoc
     * @return \common\models\FD[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\FD|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
