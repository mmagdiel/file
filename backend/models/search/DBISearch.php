<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DBI;

/**
 * DBISearch represents the model behind the search form about `common\models\DBI`.
 */
class DBISearch extends DBI
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'data_rank', 'data_position', 'data_rank_score', 'created_at', 'created_by', 'updated_at', 'updated_by', 'region_id'], 'integer'],
            [['data_position_score'], 'number'],
            [['by', 'from', 'type', 'trend', 'comments'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DBI::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'data_rank' => $this->data_rank,
            'data_position' => $this->data_position,
            'data_position_score' => $this->data_position_score, 
            'data_rank_score' => $this->data_rank_score, 
            'from' => $this->from,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'region_id' => $this->region_id,
        ]);

        $query->andFilterWhere(['like', 'by', $this->by])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'trend', $this->trend])
            ->andFilterWhere(['like', 'comments', $this->comments]);

        return $dataProvider;
    }
}
