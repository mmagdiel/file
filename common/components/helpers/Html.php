<?php 
namespace common\components\helpers;

use yii\db\Query;
use yii\helpers\BaseHtml;

class Html extends BaseHtml
{
	/**
     *
     */
    public static function title($a)
    {
        $meta = (new Query())
            ->select('name')
            ->from('metadata')
            ->where(['acronym' => $a])
            ->all();
        return $meta[0]['name'];
    }
} 