<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FDI;

/**
 * FDISearch represents the model behind the search form about `common\models\FDI`.
 */
class FDISearch extends FDI
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'region_id'], 'integer'],
            [['data'], 'number'],
            [['data_unit', 'by', 'from', 'type', 'trend', 'comments'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FDI::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'data' => $this->data,
            'from' => $this->from,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'region_id' => $this->region_id,
        ]);

        $query->andFilterWhere(['like', 'data_unit', $this->data_unit])
            ->andFilterWhere(['like', 'by', $this->by])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'trend', $this->trend])
            ->andFilterWhere(['like', 'comments', $this->comments]);

        return $dataProvider;
    }
}
