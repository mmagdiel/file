<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "mta".
 *
 * @property string $id
 * @property string $descriptive
 * @property string $to
 * @property string $region_id
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property Region $region
 */
class Mta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mta';
    }

    /**
     *  Public doc   
     */    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descriptive', 'to', 'region_id'], 'required'],
            [['region_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['descriptive'], 'string', 'max' => 997],
            [['to'], 'string', 'max' => 97],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'descriptive' => Yii::t('app', 'Descriptive'),
            'to' => Yii::t('app', 'To'),
            'region_id' => Yii::t('app', 'Region ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MtaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MtaQuery(get_called_class());
    }
}
