<?php

use common\components\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\FDI */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Comercial'), 'url' => ['site/comercial']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Detalles'), 'url' => ['region/tcomercial', 'id' => 5]];
$this->params['breadcrumbs'][] = ['label' => Html::title('fdi'), 'url' => ['region/comercial', 'id' => 5]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fdi-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'data',
            'data_unit',
            'by',
            'from',
            'type',
            'trend',
            'comments',
            'created_at:datetime',
            [
                'attribute' => 'created_by',
                'value' => function($dataProvider) {
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            'updated_at:datetime',
            [
                'attribute' => 'updated_by',
                'value' => function($dataProvider) {
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            [
                'attribute' => 'region',
                'value' => function($model) {
                    return $model->region->name;
                },
            ]
        ],
    ]) ?>

</div>
