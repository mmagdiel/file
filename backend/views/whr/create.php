<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WHR */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('WHR');
$this->params['breadcrumbs'][] = ['label' => Html::title('WHR'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="whr-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
