<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DBI */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('DBI');
$this->params['breadcrumbs'][] = ['label' => Html::title('DBI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dbi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
