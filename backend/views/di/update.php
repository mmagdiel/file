<?php

use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DI */

$this->title = Yii::t('app', 'Update') . ' ' . Html::title('DI');
$this->params['breadcrumbs'][] = ['label' => Html::title('DI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'region_id' => $model->region_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="di-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
