<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MSM */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('MSM');
$this->params['breadcrumbs'][] = ['label' => Html::title('MSM'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msm-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
