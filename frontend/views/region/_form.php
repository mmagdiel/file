<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Region */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="region-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'capital')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'area')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'map')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'flag')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'north')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'east')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'west')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'south')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'north_east')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'north_west')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'south_east')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'south_west')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'border')->textInput() ?>

    <?= $form->field($model, 'coastline')->textInput() ?>

    <?= $form->field($model, 'internet_domain')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'area_code')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_at')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency_id')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
