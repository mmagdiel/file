<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ICI */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('ICI');
$this->params['breadcrumbs'][] = ['label' => Html::title('ICI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ici-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
