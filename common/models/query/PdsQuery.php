<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\PDS]].
 *
 * @see \common\models\PDS
 */
class PdsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     *
     */
    public function current()
    {
        return $this->addOrderBy('from DESC');
    }

    /**
     * @inheritdoc
     * @return \common\models\PDS[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\PDS|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
