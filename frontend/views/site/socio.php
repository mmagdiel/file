<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Collapse;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $name ?></h1>

        <p class="lead"> <?= $defe ?></p>
    </div>

    <?php 
        $a = [];
        foreach($deta as $key => $value){
            $a[] = [
                'label' => $value['name'],
                'content' => [
                    $value['map'],
                    Html::a('Ir', Url::to(['region/tsocio', 'id' => $value['id']]), 
                        ['class' => 'btn btn-success'])
                ],
                'options' => [ 'class' => 'panel-primary' ]
            ];
        }
    ?>

    <div class="body-content">

    <?= Collapse::widget([
            'items' => $a
        ]);
    ?>
    </div>
</div>

