<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DI */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('DI');
$this->params['breadcrumbs'][] = ['label' => Html::title('DI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="di-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
