<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\RVT]].
 *
 * @see \common\models\RVT
 */
class RvtQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     *
     */
    public function current()
    {
        return $this->addOrderBy('from DESC');
    }

    /**
     * @inheritdoc
     * @return \common\models\RVT[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\RVT|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
