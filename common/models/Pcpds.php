<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "pc_pds".
 *
 * @property string $id
 * @property double $data
 * @property string $data_unit
 * @property string $by
 * @property string $from
 * @property string $type
 * @property string $trend
 * @property string $comments
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $region_id
 *
 * @property Region $region
 */
class Pcpds extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pc_pds';
    }

    /**
     *  Public doc   
     */    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data'], 'number'],
            [['data_unit', 'type', 'trend'], 'string'],
            [['by', 'from', 'type', 'trend', 'region_id'], 'required'],
            [['from'], 'safe'],
            [['created_at', 'created_by', 'updated_at', 'updated_by', 'region_id'], 'integer'],
            [['by'], 'string', 'max' => 127],
            [['comments'], 'string', 'max' => 397],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'data' => Yii::t('app', 'Data'),
            'data_unit' => Yii::t('app', 'Data Unit'),
            'by' => Yii::t('app', 'By'),
            'from' => Yii::t('app', 'From'),
            'type' => Yii::t('app', 'Type'),
            'trend' => Yii::t('app', 'Trend'),
            'comments' => Yii::t('app', 'Comments'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'region_id' => Yii::t('app', 'Region ID'),
        ];
    }

    /**
     *
     */
    public static function unit($value) 
    {
        switch ($value) {
            case "unidad":
                return " ";
                break;
            case "mil":
                return " de miles";
                break;
            case "millón":
                return " de millones";
                break;
            case "billón":
                return " de billones";
                break;
            case "trillón":
                return " de trillones";
                break;
            case "cuatrillón":
                return " de cuatrillones";
                break;
            case "quintillón":
                return " de quintillones";
                break;
            case "millardo":
                return " de millardos";
                break;
            case "billardo":
                return " de billardos";
                break;
        }
    }

    /**
     *
     */
    public static function de()
    {
        return 'No se dispone de información.';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     *
     */
    public function getDatas()
    {
        $temp = $this->data;
        if ( !is_null($temp) ) {
            $text = Yii::$app->formatter->asDecimal( $this->data ) 
                . self::unit($this->data_unit) . ' $';
            return $text;
        } else {
            return self::de();
        }
    }

    /**
     * @inheritdoc
     * @return \common\models\query\PcPdsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\PcPdsQuery(get_called_class());
    }
}
