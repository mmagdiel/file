<?php

use common\components\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\Region */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Regions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'capital',
            'area:km2',
            'map:raw',
            [
                'attribute' => 'img',
                'format' => 'html',
                'label' => 'flat',
                'value' => function ($dataProvider) {
                    return Html::img($dataProvider->imageSrc);
                },
            ],
            'north',
            'east',
            'west',
            'south',
            'north_east',
            'north_west',
            'south_east',
            'south_west',
            'border:km',
            'coastline:km',
            'internet_domain',
            'area_code',
            'created_at:datetime',
            [
                'attribute'=>'created_by',
                'value'=>function($dataProvider){
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            'updated_at:datetime',
            [
                'attribute'=>'updated_by',
                'value'=>function($dataProvider){
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            /*
            [
                'attribute' => 'region_id',
                'label' => Yii::t('app', 'Currency'),
                'value' => function($model){
                    return $model->currency->name;
                },
            ]
            */
        ],
    ]) ?>

</div>
