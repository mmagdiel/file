<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GRDI */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('GRDI');
$this->params['breadcrumbs'][] = ['label' => Html::title('GRDI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grdi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
