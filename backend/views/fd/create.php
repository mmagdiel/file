<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FD */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('FD');
$this->params['breadcrumbs'][] = ['label' => Html::title('FD'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fd-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
