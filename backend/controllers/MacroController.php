<?php

namespace backend\controllers;

use Yii;
use common\models\Macro;
use backend\models\search\MacroSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * MacroController implements the CRUD actions for Macro model.
 */
class MacroController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Macro models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MacroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Macro model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Macro model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            throw new UnauthorizedHttpException("Error!!!!");
        }

        $model = new Macro();
        $this->handle($model);
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Macro model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            throw new UnauthorizedHttpException("Error!!!!");
        }

        $model = $this->findModel($id);
        $this->handle($model);

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Macro model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Macro model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Macro the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Macro::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Upload file to the system
     */ 
    protected function handle(Macro $model)
    {
        if ($model->load(Yii::$app->request->post()) ) {
            $model->file = UploadedFile::getInstance($model,'file');
            if (!isset($model->file)) {
                throw new NotFoundHttpException("Error Processing the File");
            }
            $ext = $model->file->extension;
            print("<pre>".print_r($ext,true)."</pre>");
            if(strcmp($ext,'pdf') != 0 ) {
                throw new UnsupportedMediaTypeHttpException("Error Processing Request");
            }
            $token = Yii::$app->security->generateRandomString(23);
            $model->path = 'pdf/macro/' . $token . '.' . $ext;
            $model->file->saveAs($model->path);
            $model->error = $model->file->error;
            if ($model->error != '0') {
                throw new NotFoundHttpException("Error Saved the File");
            }
            $model->size = $model->file->size;
            $model->name = $model->file->name;
            if ($model->save(false) ) {
                $id = $model->id;
                return $this->redirect(['view', 
                   'id' => $id ]);                  
            }
        } 
    }
}
