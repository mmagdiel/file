<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\BT]].
 *
 * @see \common\models\BT
 */
class BtQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     *
     */
    public function current()
    {
        return $this->addOrderBy('from DESC');
    }

    /**
     * @inheritdoc
     * @return \common\models\BT[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\BT|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
