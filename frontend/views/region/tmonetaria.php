<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use kartik\popover\PopoverX;
use kartik\icons\Icon;
Icon::map($this);  
$meta = ArrayHelper::index($meta,'acronym');

/* @var $this yii\web\View */
/* @var $model common\models\Region */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monetarias'), 'url' => ['site/monetaria']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <h3><a class ='btn btn-success' href = <?= Url::to(['region/monetaria', 'id' => $id]) ?> >Ver detalles</a></h3>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['ms']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['ms']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['ms']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
        <?= $model->tm ?> 
            <a href = <?= isset($model->m) ? Url::to(['region/ms', 'id' => $model->m->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->m ? $model->m->commentss : '' ?></em>
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['ml']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['ml']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['ml']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tml ?>
            <a href = <?= isset($model->ml) ? Url::to(['region/ml', 'id' => $model->ml->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->ml ? $model->ml->commentss : '' ?></em>
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['mw']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['mw']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['mw']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    
    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tmw ?>
            <a href = <?= isset($model->mw) ? Url::to(['region/mw', 'id' => $model->mw->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->mw ? $model->mw->commentss : '' ?></em>
        </div> 
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['trm']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['trm']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['trm']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
        <?= $model->ttrm ?>
            <a href = <?= isset($model->trm) ? Url::to(['region/trm', 'id' => $model->trm->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->trm ? $model->trm->commentss : '' ?></em>
        </div>  
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['ier']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['ier']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['ier']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tier ?>
            <a href = <?= isset($model->ier) ? Url::to(['region/ier', 'id' => $model->ier->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->ier ? $model->ier->commentss : '' ?></em>
        </div>
    </div>

</div>
