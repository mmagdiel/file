<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FWISearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Html::title('FWI');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fwi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create') . ' ' . Html::title('FWI'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'data_rank',
            'data_position',
            'total_quantity',
            'status',
            'by',
            'from',
            // 'type',
            // 'trend',
            'comments:textShort',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            [
                'attribute' => 'region_id',
                'label' => Yii::t('app', 'Region Name'),
                'value' => 'region.name'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
