<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "mpfdb".
 *
 * @property string $id
 * @property string $descriptive
 * @property string $by
 * @property string $from
 * @property string $comments
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $region_id
 *
 * @property Region $region
 */
class Mpfdb extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mpfdb';
    }

    /**
     *  Public doc   
     */    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['by', 'from', 'region_id'], 'required'],
            [['from'], 'safe'],
            [['created_at', 'created_by', 'updated_at', 'updated_by', 'region_id'], 'integer'],
            [['descriptive'], 'string', 'max' => 997],
            [['by'], 'string', 'max' => 127],
            [['comments'], 'string', 'max' => 397],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'descriptive' => Yii::t('app', 'Descriptive'),
            'by' => Yii::t('app', 'By'),
            'from' => Yii::t('app', 'From'),
            'comments' => Yii::t('app', 'Comments'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'region_id' => Yii::t('app', 'Region ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     *
     */
    public function getCommentss()
    {
        $this->comments ? $a = '* ' . $this->comments : $a = $this->comments;
        return $a;
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MpfdbQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MpfdbQuery(get_called_class());
    }
}
