<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use kartik\popover\PopoverX;
use kartik\icons\Icon;
Icon::map($this);  
$meta = ArrayHelper::index($meta,'acronym');

/* @var $this yii\web\View */
/* @var $model common\models\Region */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fiscal'), 'url' => ['site/fiscal']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <h3><a class ='btn btn-success' href = <?= Url::to(['region/fiscal', 'id' => $id]) ?> >Ver detalles</a></h3>    

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['etrcp']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['etrcp']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['etrcp']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tetrcp ?>
            <a href = <?= isset($model->etrcp) ? Url::to(['region/etrcp', 'id' => $model->etrcp->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->etrcp ? $model->etrcp->commentss : '' ?></em>  
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['rit']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['rit']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['rit']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->trit ?>
            <a href = <?= isset($model->rit) ? Url::to(['region/rit', 'id' => $model->rit->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a> 
            <br>
            <em><?= $model->rit ? $model->rit->commentss : '' ?></em>         
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['rvt']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['rvt']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['rvt']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->trvt ?>
            <a href = <?= isset($model->rvt) ? Url::to(['region/rvt', 'id' => $model->rvt->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a> 
            <br>
            <em><?= $model->rvt ? $model->rvt->commentss : '' ?></em> 
        </div> 
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['str']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['str']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['str']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tstr ?>
            <a href = <?= isset($model->str) ? Url::to(['region/str', 'id' => $model->str->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>   
            <br>
            <em><?= $model->str ? $model->str->commentss : '' ?></em>   
        </div>
    </div>

</div>
