<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\popover\PopoverX;
$meta = ArrayHelper::index($meta,'acronym');

/* @var $this yii\web\View */
/* @var $model common\models\Region */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Macroeconomicas'), 'url' => ['site/macro']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Detalles'), 'url' => ['region/tmacro', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['gdp']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['gdp']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['gdp']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataGdp,
        'columns' => [
            
            'data',
            'data_unit',
            'data_percentage',
            'data_local',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/gdp', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['pc_gdp']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['pc_gdp']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['pc_gdp']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataPcgdp,
        'columns' => [
            
            'data',
            'data_unit',
            'data_local',
            'data_local_unit',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/pcgdp', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['pd']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['pd']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['pd']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataPd,
        'columns' => [
            
            'data',
            'data_unit',
            'data_percentage',
            'data_local',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/pd', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>   

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['pc_pd']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['pc_pd']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['pc_pd']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataPcpd,
        'columns' => [
            
            'data',
            'data_unit',
            'data_local',
            'data_local_unit',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/pcpd', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>   

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['ir']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['ir']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['ir']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataIr,
        'columns' => [
            
            'data_percentage',
            'by',
            'from',
            'type',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/ir', 'id' => $model->id]);
                    }
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['ur']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['ur']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['ur']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataUr,
        'columns' => [
            
            'data_percentage',
            'by',
            'from',
            'type',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/ur', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['inr']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['inr']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['inr']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataInr,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'data',
            'data_unit',
            'data_percentage',
            'by',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/inr', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>   

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['bt']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['bt']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['bt']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataBt,
        'columns' => [
            
            'data',
            'data_unit',
            'by',
            'from',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/bt', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>   

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['fd']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['fd']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['fd']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataFd,
        'columns' => [
            
            'data',
            'data_unit',
            'data_percentage',
            'data_local',     
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/fd', 'id' => $model->id]);
                    }
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
