<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Region;

/**
 * RegionSearch represents the model behind the search form about `common\models\Region`.
 */
class RegionSearch extends Region
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'area', 'border', 'coastline', 'area_code', 'created_at', 'created_by', 'updated_at', 'updated_by', 'currency_id'], 'integer'],
            [['name', 'capital', 'map', 'flag', 'north', 'east', 'west', 'south', 'north_east', 'north_west', 'south_east', 'south_west', 'internet_domain'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Region::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'area' => $this->area,
            'border' => $this->border,
            'coastline' => $this->coastline,
            'area_code' => $this->area_code,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'currency_id' => $this->currency_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'capital', $this->capital])
            ->andFilterWhere(['like', 'map', $this->map])
            ->andFilterWhere(['like', 'flag', $this->flag])
            ->andFilterWhere(['like', 'north', $this->north])
            ->andFilterWhere(['like', 'east', $this->east])
            ->andFilterWhere(['like', 'west', $this->west])
            ->andFilterWhere(['like', 'south', $this->south])
            ->andFilterWhere(['like', 'north_east', $this->north_east])
            ->andFilterWhere(['like', 'north_west', $this->north_west])
            ->andFilterWhere(['like', 'south_east', $this->south_east])
            ->andFilterWhere(['like', 'south_west', $this->south_west])
            ->andFilterWhere(['like', 'internet_domain', $this->internet_domain]);

        return $dataProvider;
    }
}
