<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\RegionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="region-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'capital') ?>

    <?= $form->field($model, 'area') ?>

    <?= $form->field($model, 'map') ?>

    <?php // echo $form->field($model, 'flag') ?>

    <?php // echo $form->field($model, 'north') ?>

    <?php // echo $form->field($model, 'east') ?>

    <?php // echo $form->field($model, 'west') ?>

    <?php // echo $form->field($model, 'south') ?>

    <?php // echo $form->field($model, 'north_east') ?>

    <?php // echo $form->field($model, 'north_west') ?>

    <?php // echo $form->field($model, 'south_east') ?>

    <?php // echo $form->field($model, 'south_west') ?>

    <?php // echo $form->field($model, 'border') ?>

    <?php // echo $form->field($model, 'coastline') ?>

    <?php // echo $form->field($model, 'internet_domain') ?>

    <?php // echo $form->field($model, 'area_code') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'currency_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
