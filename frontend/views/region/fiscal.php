<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\popover\PopoverX;
$meta = ArrayHelper::index($meta,'acronym');
$content =  'asfasdf afdsaf fg';

/* @var $this yii\web\View */
/* @var $model common\models\Region */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fiscal'), 'url' => ['site/fiscal']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Detalles'), 'url' => ['region/tfiscal', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['etrcp']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['etrcp']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['etrcp']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataEtrcp,
        'columns' => [
            
            'data_percentage',
            'by',
            'from',
            'type',
            'trend',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/etrcp', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['rit']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['rit']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['rit']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataRit,
        'columns' => [

            'data_percentage',
            'by',
            'from',
            'type',
            'trend',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/rit', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>   

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['rvt']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['rvt']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['rvt']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataRvt,
        'columns' => [
           
            'data_percentage',
            'by',
            'from',
            'type',
            'trend',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/rvt', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>   

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['str']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['str']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['str']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataStr,
        'columns' => [
            
            'descriptive:textShort',
            'by:textShort',
            'from',      
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/str', 'id' => $model->id]);
                    }
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
