<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PD */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('PD');
$this->params['breadcrumbs'][] = ['label' => Html::title('PD'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pd-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
