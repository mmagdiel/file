<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GPI */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('GPI');
$this->params['breadcrumbs'][] = ['label' => Html::title('GPI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gpi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
