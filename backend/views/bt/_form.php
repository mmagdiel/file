<?php

use yii\helpers\Html;
use common\models\Region;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\BT */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bt-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'data')->textInput() ?>

    <?= $form->field($model, 'data_unit')->dropDownList([ 'unidad' => 'Unidad', 'mil' => 'Mil', 'millón' => 'Millón', 'billón' => 'Billón', 'trillón' => 'Trillón', 'cuatrillón' => 'Cuatrillón', 'quintillón' => 'Quintillón', 'millardo' => 'Millardo', 'billardo' => 'Billardo', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'by')->textInput(['maxlength' => true]) ?>
    
    <?= DatePicker::widget([
        'model' => $model,
        'attribute' => 'from',
        'template' => '{addon}{input}',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
    ]);?>

    <?= $form->field($model, 'type')->dropDownList([ 'Proyectado' => 'Proyectado', 'Reportado' => 'Reportado', ], ['prompt' => 'Seleccione un Tipo']) ?>

    <?= $form->field($model, 'trend')->dropDownList([ 'Aumenta' => 'Aumenta', 'Disminuye' => 'Disminuye', 'Se Mantiene' => 'Se Mantiene', 'Sin Registro' => 'Sin Registro', ], ['prompt' => 'Seleccione una Tendecia']) ?>

    <?= $form->field($model, 'comments')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'region_id')->dropDownList(
        ArrayHelper::map(Region::find()->all(),'id', 'name'),
        ['prompt'=>'Seleccione una region'])->label(Yii::t('app','Region Name'))  
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
