<?php

use common\components\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\IR */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Macroeconomicas'), 'url' => ['site/macro']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Detalles'), 'url' => ['region/tmacro', 'id' => 4]];
$this->params['breadcrumbs'][] = ['label' => Html::title('ir'), 'url' => ['region/macro', 'id' => 4]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ir-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'data_percentage',
            'by',
            'from',
            'type',
            'trend',
            'comments',
            'created_at:datetime',
            [
                'attribute' => 'created_by',
                'value' => function($dataProvider) {
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            'updated_at:datetime',
            [
                'attribute' => 'updated_by',
                'value' => function($dataProvider) {
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            [
                'attribute' => 'region',
                'value' => function($model) {
                    return $model->region->name;
                },
            ]
        ],
    ]) ?>

</div>
