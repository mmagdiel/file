<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MW */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('MW');
$this->params['breadcrumbs'][] = ['label' => Html::title('MW'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mw-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
